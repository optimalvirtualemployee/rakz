<?php
/**
 * Template Name: Front Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. 
 *
 * @package pounce
 * @subpackage pounce-child
 */
get_header(); ?>

    <div id="content" role="main">
        <?php //get_template_part( 'template-parts/header/section', 'hero-text-large-left' ); ?>
        <?php get_template_part( 'template-parts/header/section', 'hero-slider' ); ?>

        <?php get_template_part( 'template-parts/content/section', 'two-homepage' ); ?>
        
        <!-- FAVOURTITE WORK -->
        <div id="favourite-work" class="pt-sm-5 pb-3">
          <div class="container">
            <div class="row">
              <div class="col-sm-9 offset-sm-1">
                <h2 class="h2__text-pangram text-black">Our</h2>
                <h2 class="h2__text-holiday text__primary pos__work">Favourite Work</h2>
              </div>
              <div class="col-sm-7 offset-sm-1 pr-5 mr-4">
                <p>We could point out the hard work and creativity that went into each of these projects. But we’d rather let the work speak for itself.</p>
              </div>
            </div>
          </div>
        </div> <!-- favourite-work -->
  
        
        <?php get_template_part( 'template-parts/content/section', 'portfolio' ); ?>

        <div id="our-services" class="py-5">
          <div class="container">
           
            <div class="row pt-5">
              <div class="col-md-6">
                <h2><span class="h2__text-holiday text__primary">Our</span><span class="h2__text-pangram pos_services">Services</span></h2>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6">
                <div class="pt-4">
                  <div>
                    <p>We are not just creatives. We are conduits. We digest our networks, our cultures, our experiences. We distil them. And we produce potent, creative solutions for your business that encompass everything we are.</p>
                    <p class="pt-2">Every job we take involves some, or all, of the following:<br><span class="font-weight-bold">Discover, Deliver, and Drive.</span></p>
                  </div>

                  <div class="text__hidden-below-lg text__more-top">
                    <p class="pt-2">Put simply, we take the time to <span class="font-weight-bold">discover</span> your current state and create a <span class="font-weight-bold">strategic roadmap</span> for you. We can <span class="font-weight-bold">deliver</span> any creative concepts and develop any technical solutions needed, broadly categorised into creative, marketing, and technology. Once all the moving parts have been put together, we like to stick around and take it for a <span class="font-weight-bold">drive</span>. That is, we like to make sure it’s all working smoothly, adjust the course if needed, and optimise some more.</p>
                    <p class="pt-2">Engage us for one option or all three, the choice is yours.</p>
                  </div>
                  <div class="btn button_main mt-4 d-none d-lg-inline-block">
                    <div class="button__black button__design">
                      <a href="<?php echo get_site_url() ?>/services/" role="button">Unleash your Marketing</a>
                    </div>
                  </div> <!-- button -->

                   <div class="text-center text-md-left">
                     <div class="btn button_main mt-4 d-lg-none btn__more-top">
                      <div class="button__more">
                        <a href="Javascrip:void(0);" role="button">Show More</a>
                      </div>
                    </div> <!-- button -->
                  </div>

                </div>
              </div>
              <div class="col-lg-2 text-center ellipses pt-5 pt-lg-4">
                 <a href="<?php echo get_site_url() ?>/services/creativity/"><div class="cross_creative homepage"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/Creative_Ellipse.png" alt="Creative"></div></a>
                 <h4 class="h4__pangram pt-4"><a href="<?php echo get_site_url() ?>/services/creativity/" class="text-black">Creative</a></h4>
              </div>
              <div class="col-lg-2 text-center ellipses pos__technology_ellipse">
                 <a href="<?php echo get_site_url() ?>/services/technology/" ><div class="cross_technology homepage"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/Technology_Ellipse.png" alt="Technology"></div></a>
                 <h4 class="h4__pangram pos_text-technology pt-4"><a href="<?php echo get_site_url() ?>/services/technology/" class="text-black">Technology</a></h4>
              </div>
              <div class="col-lg-2 text-center ellipses pt-lg-4">
                 <a href="<?php echo get_site_url() ?>/services/marketing/" ><div class="cross_marketing homepage"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/Marketing_Ellipse.png" alt="Marketing"></div></a>
                 <h4 class="h4__pangram pt-4"><a href="<?php echo get_site_url() ?>/services/marketing/" class="text-black" >Marketing</a></h4>
              </div>
            </div> <!-- row -->

            <div class="row">
              <div class="col-12 text-center text-lg-left">
                 <div class="btn button_main mt-4 d-lg-none mx-auto">
                    <div class="button__black button__design">
                      <a href="#" role="button">Unleash your Marketing</a>
                    </div>
                  </div> <!-- button -->
                
              </div>
            </div>

          </div>
        </div> <!-- our-services -->

        <?php get_template_part( 'template-parts/content/section', 'testimonial-carousel' ); ?>
      

        <div id="footer__secondary-large" class="pt-5">
            <div class="container">
              <div class="row pt-5">
                <div class="col-md-12 text-center">
                  <h2 class="py-5 text-center text-white h1__text-holiday">We're Ready<br>Are you?</h2>
                   <div class="btn button_main mt-4">
                     <div class="button__white button__design">
                       <a href="<?php echo get_site_url() ?>/contact/" role="button">Let's Do it</a>
                     </div>
                  </div>
                </div> <!-- col-12 -->
              </div> <!-- row -->
          </div> <!-- container -->
        </div> <!--footer__secondary-large -->


    </div><!-- #content -->
  </div><!-- #primary -->
  
<?php get_footer(); ?>