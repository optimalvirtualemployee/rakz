<?php 

if ( is_front_page() ) {
  $hero__tagline = get_field( "hero_large_left_heading" );
  
  $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); 
  $backgroundImg = $backgroundImg[0];

  $backgroundMobileImg = get_field( "mobile_hero_image" ); 
}

else if ( is_404() ){
    $backgroundImg = get_stylesheet_directory_uri() . '/img/hero-404.png';
    $backgroundMobileImg = get_stylesheet_directory_uri() . '/img/hero-mobile-404.png';
}

?>

<style>
  
  @media (max-width: 575px){
    .bg-image {
      background-image: url("<?php echo $backgroundMobileImg; ?>");
    }
  }
   @media (min-width: 576px){
    .bg-image {
      background-image: url("<?php echo $backgroundImg ?>");
    }
  }
</style>

<div class="main-carousel slider">
<div class="main-item">
<div class="hero-section bg-image jumbotron jumbotron-fluid mb-0">
     <div class="container h-100">
     
         
              <?php if ( is_front_page() ) { ?>
               
                  <div class="row h-100 align-items-sm-center">
                   <div class="col-12 text__hero-position">
                      <h1 class="h1__text-pangram text-white"><?php echo $hero__tagline ?></h1>
                      <!-- <div class="btn button_main mt-4">
                        <div class="button__black button__design">
                        <a href="#" class="btn cta-btn">Contact Us</a>
                        </div>
                      </div> -->
                      
                      <div class="scroll-icon pt-5 pb-1 pt-md-7 text-center text-sm-left">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/scroll-icon.svg" class="pos__scroll-icon" alt="Scroll Down">
                        <br><span class="text-white">Scroll Down</span>
                      </div>
                    </div>
                    </div> </div>
                    </div> </div> 
                    <!-- End item -->
                    <div class="main-item">
<div class="hero-section bg-image jumbotron jumbotron-fluid mb-0">
     <div class="container h-100">
                    <div class="row h-100 align-items-sm-center">
                   <div class="col-12 text__hero-position">
                      <h1 class="h1__text-pangram text-white"><?php echo $hero__tagline ?></h1>
                      <div class="btn button_main mt-4">
                        <div class="button__black button__design">
                        <a href="#" class="btn cta-btn">Contact Us</a>
                        </div>
                      </div>
                      <div class="scroll-icon pb-1 pt-5 pt-md-7 text-center text-sm-left">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/scroll-icon.svg" class="pos__scroll-icon" alt="Scroll Down">
                        <br><span class="text-white">Scroll Down</span>
                      </div>
                    </div>
                    </div>
                    </div> 
                    <!-- End item -->
                  </div> 
                </div>
              </div>
                <!-- End main-carousel -->
                <?php } else if ( is_404() ){ ?>
                  <div class="hero-section bg-image jumbotron jumbotron-fluid mb-0">
     <div class="container h-100">
     <div class="row h-100 align-items-sm-center">
                  <div class="col-sm-6 text__hero-position">
                    <h1 class="h1__text-pangram text-white pb-3">OUCH 404</h1>
                    <p class="text-white">We seem to have missed our footing here.<br><span class="text-aktiv font-weight-bold">Don’t stress - we’re working on it.</span></p>
                    <p class="text-white">In the meantime, you shoot us a message or head back to the home page.</p>
                    <div class="btn button_main mt-0 mt-lg-4">
                        <div class="button__white button__design">
                          <a href="<?php echo get_site_url() ?>" role="button">Homepage</a>
                        </div>
                      </div> <!-- button -->
                  </div>  </div> <!-- button -->
                  </div> </div>
                <?php } else {} ?>
              
          
     
</div>  <!-- hero-section--> 

