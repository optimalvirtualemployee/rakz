<?php
/**
 * Template Name: Services Inner
 * description: >- Creativity/Marketing/Technology inner pages
 *
 * @package pounce
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header(); 

$container = get_theme_mod( 'understrap_container_type' );

?>

<!-- ACF -->
<?php 

$services_inner_top_text_left = get_field( "services_inner_top_text_left" ); 
$services_inner_top_text_right = get_field( "services_inner_top_text_right" );
$services_inner_top_section_tagline = get_field( "services_inner_top_section_tagline" );

$services_inner_second_section_heading = get_field( "services_inner_second_section_heading" );
$services_inner_second_section_content = get_field( "services_inner_second_section_content" );
$services_inner_top_bg_image = get_field( "services_inner_top_bg_image" );

$services_inner_top_section_button_text = get_field( "services_inner_top_section_button_text" );
$services_inner_bottom_left_content = get_field( "services_inner_bottom_left_content" );
$services_inner_bottom_right_content = get_field( "services_inner_bottom_right_content" );

$services_inner_bottom_bg_image = get_field( "services_inner_bottom_bg_image" );

?>


  <div id="primary" class="site-content">
    <div id="content" role="main">

    	<div id="services-inner">

	    	<?php get_template_part( 'template-parts/header/section', 'hero-half-screen-text-center' ); ?>

	    	<div class="container py-5">
			    <div class="row pt-md-5 pb-2">
			    	<div class="col-md-5 offset-md-1">
			    		<?php echo $services_inner_top_text_left ?>
			    	</div>
			    	<div class="col-md-5 text__hidden-below-md text__more-top">
			    		<?php echo $services_inner_top_text_right ?>
			    	</div>

	    		</div> <!-- row -->

	    		<div class="row">
	    		  <div class="text-center text-md-left col-12">
		                 <div class="btn button_main mb-4 d-md-none btn__more-top">
		                  <div class="button__more">
		                    <a href="Javascrip:void(0);" role="button">Show More</a>
		                  </div>
		                </div> <!-- button -->
	              </div>
	    		</div> <!-- row -->

	    		<div class="row pt-4 pb-3 pb-lg-0">
			    	<div class="col-md-12 <?php if (is_page('technology')): ?> d-block text-center <?php  else: ?>  d-flex justify-content-center <?php endif ?>  ">
			    		<?php echo $services_inner_top_section_tagline ?>
			    	</div>
	    		</div> <!-- row -->

	    		<div class="row pt-0 pb-5">
			    	<div class="col-md-12 text-center">
			    		<div class="btn button_main mt-0 mt-lg-4">
		                    <div class="button__black button__design">
		                      <a href="<?php echo get_site_url() ?>/contact-us" role="button"><?php echo $services_inner_top_section_button_text ?></a>
		                    </div>
                    	</div> <!-- button -->
			    	</div>
	    		</div> <!-- row -->
	    	</div> <!-- cntainer -->


	    	<div class="bg-right-circles" style="background-image: url('<?php echo $services_inner_top_bg_image; ?>');">
		    	<div class="container py-3">
				    <div class="row pt-md-5 pb-2">


				    	<div class="offset-md-1 <?php if (is_page('technology')): ?> col-md-6 <?php  else: ?>  col-md-5 <?php endif ?> <?php if (is_page('technology')): ?> d-block <?php  else: ?>  d-flex <?php endif ?>">
				    		<?php echo $services_inner_second_section_heading ?>
				    	</div>
		    		</div> <!-- row -->
		    		<div class="row pt-4 py-0">
				    	<div class="col-md-6 offset-md-1">
				    		<?php echo $services_inner_second_section_content ?>
				    	</div>
		    		</div> <!-- row -->
		    	
		    	</div> <!-- cntainer -->
	    	</div>

	    	<?php get_template_part( 'template-parts/content/section', 'core-features' ); ?>
	    	

	    	<!-- BOTTOM SECTION -->
	    	<div class="bg-left-circles" style="background-image: url('<?php echo $services_inner_bottom_bg_image; ?>');">
		    	<div class="container pt-0 pt-lg-5">
				    <div class="row <?php if (is_page('technology')): ?>  <?php  else: ?>  pt-md-5 pb-5 <?php endif ?> text__hidden-below-lg text__more-bottom">

			    	<?php
					if ( is_page('technology') ) {
					?>
						<div class="col-lg-5 offset-lg-1 pb-lg-5">
				    		<?php echo $services_inner_bottom_left_content ?>
				    	</div>
				    	<div class="col-lg-5">
				    		<?php echo $services_inner_bottom_right_content ?>
				    	</div>
					<?php
					} 

					else { ?>

						<div class="col-lg-6 offset-lg-1 pb-lg-5">
				    		<?php echo $services_inner_bottom_left_content ?>
				    	</div>
					<?php
					}
					?>

		    		</div> <!-- row --> 
		    		<div class="row">
		    		  <div class="text-center col-12">
			                 <div class="btn button_main mb-4 d-lg-none btn__more-bottom">
			                  <div class="button__more">
			                    <a href="Javascrip:void(0);" role="button">Show More</a>
			                  </div>
			                </div> <!-- button -->
		              </div>
		    		</div> <!-- row -- >
		    	</div> <!-- cntainer -->
	    	</div>
	    	<?php
			if ( is_page('technology') ) { ?>
					<?php get_template_part( 'template-parts/content/section', 'tech-stack' ); ?>
			<?php } ?>

	    	<?php get_template_part( 'template-parts/footer/section', 'secondary-content' ); ?>  
    	</div> <!-- bg-circles -->
   
    </div><!-- #content -->
  </div><!-- #primary -->

<?php get_footer(); ?>
