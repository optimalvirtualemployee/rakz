<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>
<style>
	.a2a_kit.a2a_kit_size_32.a2a_floating_style.a2a_vertical_style {
		margin-left:-50px !important;
	}
</style>

<div class="wrapper blog__masonry" id="page-wrapper">

	<?php get_template_part( 'template-parts/header/section', 'hero-half-screen-text-center' ); ?>

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row py-5">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

				<div class="blog__masonry-grid">
					<?php query_posts(array('post_type'=>'post', 'orderby'=>'published', 'order'=>'DESC')); ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'home' ); ?>

			
					<?php endwhile; // end of the loop. ?>
				</div>
			<!-- Do the right sidebar check -->
			<?php /*get_template_part( 'global-templates/right-sidebar-check' );*/ ?>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer();
	