<div class="hero-section jumbotron jumbotron-fluid mb-0">
<div class="main-carousel slider">
 
<?php $sliders = get_field('slider');
    
    foreach($sliders as $slider){ 

      $slidertext = $slider['slider_text'];
      $sliderImg = $slider['slider_image'];
      $sliderMobileImg = $slider['slider_mobile_image'];

  ?>
<div class="main-item">
   <div class="main-img d-none d-md-block"> <img src="<?php echo $sliderImg['url'];  ?>"></div>
   <div class="main-img d-block d-md-none"> <img src="<?php  echo $sliderMobileImg['url']?>"></div>
     <div class="container h-100">
             
               
                  <div class="row h-100 align-items-sm-center">
                   <div class="col-12 text__hero-position">
                      <h1 class="h1__text-pangram text-white"><?php echo  $slidertext; ?></h1>
                      <?php if ( $slider['cta_enable_diable'] ){ 
                        $cta = $slider['cta_url'];
                      ?>
                   <div class="btn button_main mt-4">
                        <div class="button__black button__design">
                        <a href="<?php echo $cta['url']; ?>" class="btn cta-btn"><?php echo $cta['title']; ?></a>
                        </div>
                      </div> 
                    <?php }?>
                      
                      <div class="scroll-icon pt-5 pb-1 pt-md-7 text-center text-sm-left">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/scroll-icon.svg" class="pos__scroll-icon" alt="Scroll Down">
                        <br><span class="text-white">Scroll Down</span>
                      </div>
                    </div>
                    </div>
                   </div>
                  </div> 
                  <?php }?>
                 
        
                    <!-- End item -->
 
                </div>
             
                
              
          
     
</div>  <!-- hero-section--> 

