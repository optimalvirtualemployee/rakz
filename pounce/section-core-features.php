<!-- Core Features COntent -->
<div class="container py-3">
    <div class="row pt-md-3 pb-2">
  	<?php 
	if( have_rows('services_inner_core_features') ):
	    while ( have_rows('services_inner_core_features') ) : the_row();
	        $icon_url = get_sub_field('core_feature_icon');
	        $core_feature_heading = get_sub_field('core_feature_heading');
	        $core_feature_content = get_sub_field('core_feature_content');
	      	$core_feature_enable_disable_link = get_sub_field('enable_disable_link');
	      	$core_feature_enable_disable_cta = get_sub_field('enable_disable_cta');

	       
	?>

	<div class="col-md-2 col-lg-1 text-md-right py-0 py-md-3">
		
		<?php 
			if($core_feature_enable_disable_link == 1){
		?>
		<a href="<?php echo get_sub_field('icon_link'); ?>"><img src="<?php echo $icon_url ?>" /></a>
		<?php } else {?>
		<img src="<?php echo $icon_url ?>" />
	<?php }?>
	</div>
	<div class="col-md-4 col-lg-3 py-3">
		<h5 class="font-weight-bold text-aktiv"><?php echo $core_feature_heading ?></h5>
		<p><?php echo $core_feature_content ?></p>
		<div class="btn button_main mt-0 ">
		<?php
			if($core_feature_enable_disable_cta== 1){
		?>	
		<div class="button__black button__design">
		    <a href="<?php echo get_sub_field('cta'); ?>" role="button">Read More</a>
		 </div>
		 <?php
			}
		?>
        </div>
	</div>
	<?php 
	    endwhile;
	else :
	    // no rows found
	endif;

	?>
	</div> <!-- row -->

</div> <!-- cntainer -->