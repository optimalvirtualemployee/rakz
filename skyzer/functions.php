<?php
/*This file is part of skyzertheme-child, skyzertheme child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/

if ( ! function_exists( 'suffice_child_enqueue_child_styles' ) ) {
	function skyzertheme_child_enqueue_child_styles() {
		// loading parent style
		wp_register_style(
			'parente2-style',
			get_template_directory_uri() . '/style.css'
		);
		
		wp_enqueue_style( 'parente2-style' );
		// loading child style
		wp_register_style(
			'childe2-style',
			get_stylesheet_directory_uri() . '/style.css'
		);
		wp_enqueue_style( 'childe2-style');
	}
}
add_action( 'wp_enqueue_scripts', 'skyzertheme_child_enqueue_child_styles' );

//common fields
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' => 'Theme Option',
		'menu_title' => 'Theme Settings',
		'menu_slug' => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect' => false
	));
	
	acf_add_options_sub_page(array(
		'page_title' => 'Theme Header Settings',
		'menu_title' => 'Header',
		'parent_slug' => 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' => 'Theme Footer Settings',
		'menu_title' => 'Footer',
		'parent_slug' => 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' => 'Theme SiderBar Settings',
		'menu_title' => 'Sidebar',
		'parent_slug' => 'theme-general-settings',
	));
}
